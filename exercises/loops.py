from functools import reduce
import operator
"""
for x in 'programas':
    print(x)
"""

a= "Veronica"
print(a[:])

#a[6] = 'K'

#list = [print(x) for x  in range(0, 20) if x % 2 == 0]

def operation(inicial):
    total = inicial
    def square(x):
        nonlocal total
        total += x * x
        return total
    return square

o = operation(5)

print(o(3))
print(o(5))
print(o(2))

def upper_case(f):
    def wrap(*args, **kwargs):
        x = f(*args, **kwargs)
        return x.upper()
    return wrap

@upper_case
def showCity(city):
    return city

print(showCity('La Paz'))

#print([(x, y) for x in range(20) if x % 2 == 0 for y in range(x) if y % 2 == 1])
print([[y for y in range(5)] for x in range(10)])

"""
i = map(str, range(5))
print(list(i))

i = map(ord, "Hector Ramon Leon Molina")
print(list(i))
"""
multiple = map(lambda x: x * 3, range(1,15))
print(list(multiple))

positives = filter(lambda x: x% 2 == 0, range(1,15))
print(list(positives))

accumulator = reduce(lambda x, y: x + y, range(0,5))
print(accumulator)

s= "Hector Leon"
s= 'Leon'

for i in s:
    print(i)